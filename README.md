# Persiana

A slender typeface hidden behind shutters.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) in Avilés, 2017.

![Font preview](https://gitlab.com/typebits/font-persiana/-/jobs/artifacts/master/raw/Persiana-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-persiana/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-persiana/-/jobs/artifacts/master/raw/Persiana-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-persiana/-/jobs/artifacts/master/raw/Persiana-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-persiana/-/jobs/artifacts/master/raw/Persiana-Regular.sfd?job=build-font)

# Authors

This typeface was created by ESAPA students at the Type:Bits workshop in Avilés, 2017.

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
